package Assignment;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestCase2 {
	 WebDriver driver;
	    
		@BeforeTest
		public void SetUp() throws InterruptedException {
			
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
			driver.get("https://techcrunch.com/");
			Thread.sleep(2000);
			
		}
		
		@Test(priority=1, description="Verification of Titles")
		public void TitleVerification () throws InterruptedException{
			
			List<WebElement> myList = driver.findElements(By.xpath("//div[@class='river river--homepage ']//article"));
			Random rand = new Random(); 
			int value = rand.nextInt(myList.size()); 
		    //System.out.println(myList.size());
		    //System.out.println(value);
		    myList.get(value).click();
		    //Thread.sleep(2000);
		    WebDriverWait wait = new WebDriverWait(driver,10);
		    try {
		    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='river river--homepage ']//article[@class='article-container article--post']//h1[@class='article__title']")))); 
		    System.out.println(driver.getTitle());
		    String title = driver.findElement(By.xpath("//div[@class='river river--homepage ']//article[@class='article-container article--post']//h1[@class='article__title']")).getText();
		    //String title =   driver.findElement(By.xpath("//div[@class='river river--homepage ']//article["+value+"]//h1[@class='article__title']")).getText();
		    System.out.println(title);
		    System.out.println("\n");
		    if(driver.getTitle().contains(title)) {
		    	System.out.println("the browser title is the same with the news title");
		    }else {
		    	System.out.println("the browser title is not the same with the news title");
		    }}
		    catch(NoSuchElementException exception) {
		    	
		    	
		    	System.out.println(driver.getTitle());
		    	String title2 = driver.findElement(By.xpath("//div[@class='river river--homepage ']//article[@class='article-container article--post article--featured article--premium-content']//h1[@class='article__title']")).getText();
			    //String title =   driver.findElement(By.xpath("//div[@class='river river--homepage ']//article["+value+"]//h1[@class='article__title']")).getText();
			    System.out.println(title2);
			    System.out.println("\n");
			    if(driver.getTitle().contains(title2)) {
			    	System.out.println("the browser title is the same with the news title");
			    }else {
			    	System.out.println("the browser title is not the same with the news title");
			    }
		     	
			}
		        
			
		}
		@Test(priority=2, description="Verification of the links within the news content")
		public void LinkVerification () {
			
			try {
			List<WebElement> Links =driver.findElements(By.xpath("//div[@class='river river--homepage ']//article[@class='article-container article--post']//div[@class='article-content']//a[@href]"));
			//System.out.println(Links.size());
			System.out.println("Links in this article are the following ones;");
			if(Links.size()==0) {
				try {
				List<WebElement> LinksPremium = driver.findElements(By.xpath("//div[@class='river river--homepage ']//article[@class='article-container article--post article--featured article--premium-content']//div[@class='article-content']//a[@href]"));
				for(int i=0; i<LinksPremium.size(); i++) {
					System.out.println(LinksPremium.get(i).getText());
				}}
				catch(NoSuchElementException exception) {
					System.out.println("There is no link in this premium article");
				}
			}else {
				
				for(int i=0; i<Links.size(); i++) {
					System.out.println(Links.get(i).getText());}
				}}
			catch(NoSuchElementException exception) {
				System.out.println("There is no link in this article");
			}	
		}
		

}
